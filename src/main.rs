use chrono::Utc;
use csv::Writer;
use minifb::{Key, KeyRepeat, Window, WindowOptions};
use plotters::{prelude::*, series::LineSeries};
use plotters_bitmap::bitmap_pixel::BGRXPixel;
use plotters_bitmap::BitMapBackend;
use serialport::available_ports;
use spectrum_analyzer::{
    samples_fft_to_spectrum, scaling::divide_by_N_sqrt, windows::hann_window, FrequencyLimit,
};
use std::time::{SystemTime, UNIX_EPOCH};
use std::{
    borrow::{Borrow, BorrowMut},
    collections::VecDeque,
    env,
    error::Error,
    ops::Range,
};

const W: usize = 600;
const H: usize = 400;

const FREAME_RATE: f64 = 15.0;

struct BufferWrapper(Vec<u32>);
impl Borrow<[u8]> for BufferWrapper {
    fn borrow(&self) -> &[u8] {
        // Safe for alignment: align_of(u8) <= align_of(u32)
        // Safe for cast: u32 can be thought of as being transparent over [u8; 4]
        unsafe { std::slice::from_raw_parts(self.0.as_ptr() as *const u8, self.0.len() * 4) }
    }
}
impl BorrowMut<[u8]> for BufferWrapper {
    fn borrow_mut(&mut self) -> &mut [u8] {
        // Safe for alignment: align_of(u8) <= align_of(u32)
        // Safe for cast: u32 can be thought of as being transparent over [u8; 4]
        unsafe { std::slice::from_raw_parts_mut(self.0.as_mut_ptr() as *mut u8, self.0.len() * 4) }
    }
}
impl Borrow<[u32]> for BufferWrapper {
    fn borrow(&self) -> &[u32] {
        self.0.as_slice()
    }
}
impl BorrowMut<[u32]> for BufferWrapper {
    fn borrow_mut(&mut self) -> &mut [u32] {
        self.0.as_mut_slice()
    }
}
fn main() -> Result<(), Box<dyn Error>> {
    let ports = available_ports().expect("No serial ports found");
    println!("Available serial ports:");
    ports.iter().enumerate().for_each(|(n, port)| {
        println!("{}: {}", n, port.port_name);
    });
    let args: Vec<String> = env::args().collect();
    if args.len() != 3 {
        println!("Usage: {} <serial_port_number> --<mic or acc>", args[0]);
        return Ok(());
    }
    let index = usize::from_str_radix(args[1].as_str(), 10).unwrap();
    let mut port = serialport::new(ports[index].port_name.clone(), 2_000_000)
        .open()
        .expect("Failed to open port");

    let mut sample_size = 4096;
    let mut sample_rate = 20_000;
    let mut xspec: Range<f32> = 0.0_f32..10_000.0_f32;
    let mut yspec: Range<f32> = -0.0_f32..10.0_f32;
    let mut pref = "mic";
    if args[2].as_str() == "--acc" {
        sample_size = 4096;
        sample_rate = 42_000;
        xspec = 0.0_f32..20_000.0_f32;
        yspec = -0.0_f32..10.0_f32;
        pref = "acc";
    }
    match atgs[2].as_str() == "--adxl356" {
        sample_size = 4096;
        sample_rate = 5_500;
        xspec = 0.0_f32..2_000.0_f32;
        yspec = -0.0_f32..10.0_f32;
        pref = "adxl356";
    }

    let mut filename = Utc::now().format("log/%Y%m%d%H%M%S").to_string();
    filename = format!("{}_{}.csv", filename, pref);
    println!("filename: {}", filename);
    let mut wtr = Writer::from_path(filename).expect("Failed to open file");

    let mut buf = BufferWrapper(vec![0u32; W * H]);

    let mut window = Window::new("data", W, H, WindowOptions::default())?;
    let root =
        BitMapBackend::<BGRXPixel>::with_buffer_and_format(buf.borrow_mut(), (W as u32, H as u32))?
            .into_drawing_area();
    root.fill(&BLACK)?;

    let mut chart = ChartBuilder::on(&root)
        .margin(10_u32)
        .set_all_label_area_size(30_u32)
        .build_cartesian_2d(xspec, yspec)?;

    chart
        .configure_mesh()
        .label_style(("sans-serif", 15).into_font().color(&GREEN))
        .axis_style(&GREEN)
        .draw()?;

    let cs = chart.into_chart_state();
    drop(root);

    let mut samples = VecDeque::new();
    let start_ts = SystemTime::now();
    let mut last_flushed = 0.0;

    while window.is_open() && !window.is_key_down(Key::Escape) {
        let epoch = SystemTime::now()
            .duration_since(start_ts)
            .unwrap()
            .as_secs_f64();

        let mut serial_buf = [0_u8; 2];
        match port.read(serial_buf.as_mut_slice()) {
            Ok(_) => {
                let data = u16::from_be_bytes(serial_buf) as f32; // - 2048.0;
                samples.push_back(data);
                wtr.write_record(&[
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_micros()
                        .to_string(),
                    data.to_string(),
                ])?;
            }
            Err(_) => (),
        };

        if samples.len() > sample_size {
            samples.pop_front();
        }

        if epoch - last_flushed > 1.0 / FREAME_RATE && samples.len() == sample_size {
            let hann_window = hann_window(samples.make_contiguous());
            let spectrum = samples_fft_to_spectrum(
                &hann_window,
                sample_rate,
                FrequencyLimit::All,
                Some(&divide_by_N_sqrt),
            )
            .unwrap();

            let root = BitMapBackend::<BGRXPixel>::with_buffer_and_format(
                buf.borrow_mut(),
                (W as u32, H as u32),
            )?
            .into_drawing_area();
            let mut chart = cs.clone().restore(&root);
            chart.plotting_area().fill(&BLACK)?;

            chart
                .configure_mesh()
                .bold_line_style(&GREEN.mix(0.2))
                .light_line_style(&TRANSPARENT)
                .draw()?;

            // let line_series = LineSeries::new(
            //     (0..SAMPLE_SIZE)
            //         .map(|i| i as f32 / SAMPLE_SIZE as f32)
            //         .zip(samples.iter())
            //         .map(|(x, y)| (x, *y)),
            //     &RED,
            // );
            let line_series = LineSeries::new(
                spectrum.data().iter().map(|(x, y)| (x.val(), y.val())),
                &RED,
            );
            chart.draw_series(line_series)?;

            drop(root);
            drop(chart);

            window
                .get_keys_pressed(KeyRepeat::Yes)
                .iter()
                .for_each(|key| match key {
                    _ => (),
                });

            window.update_with_buffer(buf.borrow(), W, H)?;
            last_flushed = epoch;
        }
    }

    wtr.flush().expect("Failed to flush file");
    Ok(())
}
